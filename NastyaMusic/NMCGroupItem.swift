//
//  NMCGroupItem.swift
//  NastyaMusic
//
//  Created by adm on 11.05.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import CoreData

class NMCGroupItem: NMCInitModelWithDict, NMCRemoteEntity {

    static let entityRequestMethod = "groups.get"

    let id: Int
    let name: String
    let photoUrl: String?

    required init?(dict: NMCModeLDict) {
        guard let id = dict["id"] as? Int
            else {
                print("fail to init friend model with dict: " + dict.description)
                return nil
        }
        self.id = id
        self.name = (dict["name"] as? String) ?? ""
        self.photoUrl = (dict["photo_100"] as? String)
    }
}
