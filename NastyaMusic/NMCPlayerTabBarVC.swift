//
//  NMCPlayerTabBarVC.swift
//  NastyaMusic
//
//  Created by adm on 22.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit

class NMCPlayerTabBarVC: UITabBarController {

    let playerController = UIStoryboard(name: "Main", bundle: nil)
        .instantiateViewControllerWithIdentifier("storyIdPlayer")

    let playerViewHeight: CGFloat = 50.0
    var playerBottomConstraint: NSLayoutConstraint? = nil


    override func viewDidLoad() {
        super.viewDidLoad()

        bindToPlayer()
        addViewControllers()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        self .becomeFirstResponder()
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return isIPAD() ? .All : .Portrait
    }

    func bindToPlayer() {
        let player = NMCPlayer.sharedManager

        player.playerStopped.distinct().observe { [unowned self] stopped in
            if stopped {
                self.dismissPopupBarAnimated(true, completion: nil)
            } else {
                self.presentPopupBarWithContentViewController(self.playerController,
                                                              animated: true,
                                                              completion: nil)
            }
        }

    }

    func addViewControllers() {
        let friendsSplitVC = UISplitViewController()
        friendsSplitVC.tabBarItem = UITabBarItem(title: "friends", image: nil, selectedImage: nil)
        guard let story = storyboard,
        friendsSplitDetail = story
            .instantiateViewControllerWithIdentifier("storyIdSongsNavVC") as? UINavigationController
        else {return}
        let friendsSplitMaster = story
            .instantiateViewControllerWithIdentifier("storyIdFriendsNavVC")

        if let songsVM = (friendsSplitDetail.viewControllers[0] as? NMCSongsVC)?.vm as? NMCSongsVM {
            songsVM.vcType = NMCSongsVCType.Friend
        }

        friendsSplitVC.viewControllers = [friendsSplitMaster, friendsSplitDetail]

        let groupsSplitVC = UISplitViewController()
        groupsSplitVC.tabBarItem = UITabBarItem(title: "groups", image: nil, selectedImage: nil)
        let groupsSplitMaster = story.instantiateViewControllerWithIdentifier("storyIdGroupsNavVC")
        guard let groupsSplitDetail = story
            .instantiateViewControllerWithIdentifier("storyIdSongsNavVC") as? UINavigationController
        else {return}

        if let songsVM = (groupsSplitDetail.viewControllers[0] as? NMCSongsVC)?.vm as? NMCSongsVM {
            songsVM.vcType = NMCSongsVCType.Group
        }

        groupsSplitVC.viewControllers = [groupsSplitMaster, groupsSplitDetail]

        guard let mySongsNavVC = story
            .instantiateViewControllerWithIdentifier("storyIdSongsNavVC") as? UINavigationController
        else {return}
        mySongsNavVC.tabBarItem = UITabBarItem(title: "my songs", image: nil, selectedImage: nil)

        if let songsVM = (mySongsNavVC.viewControllers[0] as? NMCSongsVC)?.vm as? NMCSongsVM {
            songsVM.vcType = NMCSongsVCType.MySongs
        }

        viewControllers = [mySongsNavVC, friendsSplitVC, groupsSplitVC]

        for splitVC in [friendsSplitVC, groupsSplitVC] {
            splitVC.delegate = self
            splitVC.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        }
    }

    override func canBecomeFirstResponder() -> Bool {
        return true
    }

    override func remoteControlReceivedWithEvent(event: UIEvent?) {
        if let subtype = event?.subtype {

            switch subtype {

            case .RemoteControlPlay:
                NMCPlayer.sharedManager.play()
                print("remote play btn pressed")

            case .RemoteControlPause:
                NMCPlayer.sharedManager.pause()
                print("remote pause btn pressed")

            case .RemoteControlPreviousTrack:
                NMCPlayer.sharedManager.playPrevious()
                print("remote previos track btn pressed")

            case .RemoteControlNextTrack:
                NMCPlayer.sharedManager.playNext()
                print("remote next track btn pressed")

            default:
                break
            }
        }
    }
}

// swiftlint:disable line_length

extension NMCPlayerTabBarVC : UISplitViewControllerDelegate {
    func splitViewController(splitViewController: UISplitViewController,
                             collapseSecondaryViewController secondaryViewController: UIViewController,
                                ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return true
    }
}
// swiftlint:enable line_length
