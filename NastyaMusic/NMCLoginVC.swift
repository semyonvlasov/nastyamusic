//
//  NMCLoginVC.swift
//  NastyaMusic
//
//  Created by adm on 28.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit
import VK_ios_sdk

class NMCLoginVC: UIViewController, VKSdkUIDelegate, VKSdkDelegate {

    @IBOutlet weak var tryAuthBtn: UIButton!

    let VKAppId = "5383586"

    override func viewDidLoad() {
        super.viewDidLoad()
        let sdkInstance = VKSdk.initializeWithAppId(VKAppId)
//        VKSdk.forceLogout()
        sdkInstance.registerDelegate(self)
        sdkInstance.uiDelegate = self
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tryToConnect(self)
    }

    func goToMainScreen() {
        performSegueWithIdentifier("segueIdSuccessLogin", sender: self)
    }

    // MARK: - VK Connect functions
    @IBAction func tryToConnect(sender: AnyObject) {
        let permissions = ["notify", "audio", "groups", "offline"]
        VKSdk.wakeUpSession(permissions) { (state, error) in
            if state == VKAuthorizationState.Authorized {
                NSLog("vk already authorized")
                self.goToMainScreen()

            } else if state == VKAuthorizationState.Initialized {
                VKSdk.authorize(permissions)
            } else if error != nil {
                NSLog("vk session wake up error")
            }
        }
    }

    //MARK: - VK SDK Delegate
    func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
        if let err = result.error {
            UIAlertView(title: "Ошибка",
                        message: String(err.userInfo["NSLocalizedDescription"]),
                        delegate: nil,
                        cancelButtonTitle: "Ок")
                .show()
            tryAuthBtn.hidden = false
        } else {
            if VKSdk.vkAppMayExists() {
                tryToConnect(self)
            }
        }
    }

    func vkSdkUserAuthorizationFailed() {
        NSLog("vk authorization failed")
    }

    //MARK: - VK UI Delegate
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        presentViewController(controller, animated: true, completion: {})
    }

    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
        NSLog("vk need to enter captcha")
    }
}
