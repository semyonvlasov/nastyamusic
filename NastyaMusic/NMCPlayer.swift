//
//  NMCPlayer.swift
//  NastyaMusic
//
//  Created by adm on 26.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import AVFoundation
import Bond
import MediaPlayer

class NMCPlayer: UIResponder, HysteriaPlayerDelegate, HysteriaPlayerDataSource {
    static let sharedManager = NMCPlayer()

    let errorMessages = EventProducer<String>()
    let playerStopped = Observable<Bool>(true)
    let isPlaying = Observable<Bool>(false)
    let selectedSong = Observable<NMCSongItem?>(nil)
    let cover = Observable<UIImage?>(nil)

    let currentTime = Observable<Float>(0.0)
    var stopUpdateCurrentTime = false

    var timeObserver: AnyObject? = nil

    var nowPlayingIndex = 0

    private var player: HysteriaPlayer {  return HysteriaPlayer.sharedInstance() }
    private var playlist: [NMCSongItem] = [NMCSongItem]()

    private var infoDict = [String : AnyObject]()

    private var coversCache = NSCache()

    //MARK:- Initialization
    override init() {
        super.init()
        setupPlayer()
    }

    private func setupPlayer() {
        player.delegate = self
        player.datasource = self

        selectedSong.skip(1).observe { [unowned self] song in
            //TODO: add real album cover
            let artWork = MPMediaItemArtwork(image: UIImage(named:"headphones_large")!)
            self.infoDict[ MPMediaItemPropertyArtwork ] = artWork

            self.infoDict[ MPMediaItemPropertyTitle ] = song?.title ?? ""
            self.infoDict[ MPMediaItemPropertyArtist ] = song?.artist ?? ""
            self.infoDict[ MPMediaItemPropertyPlaybackDuration ] = song?.duration ?? 0
            self.infoDict[ MPNowPlayingInfoPropertyElapsedPlaybackTime ] = Double(0)

            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = self.infoDict
        }
    }
    
    //MARK:- External
    func pause() {
        player.pause()
        isPlaying.value = false

        self.resetRemotePlayingTime()
    }

    func play() {
        player.play()
        isPlaying.value = true
        self.resetRemotePlayingTime()
    }

    func playNext() {
        player.playNext()
    }

    func playPrevious() {
        if currentTime.value > 5 {
            player.seekToTime(0) { _ in self.resetRemotePlayingTime() }
        } else {
            player.playPrevious()
        }
    }

    func seekWithProgress(progress: Float, completion: ((Bool) -> (Void))?) {
        if let duration = selectedSong.value?.duration {
            currentTime.value = Float(duration) * progress

            player.seekToTime(Double(currentTime.value), withCompletionBlock: { success in
                self.resetRemotePlayingTime()
                if completion != nil { completion!(success) }
            })
        }
    }

    func resetWithPlaylist(newPlaylist: [NMCSongItem], andPlayIndex: Int?) {
        playerStopped.value = false
        playlist = newPlaylist
        if let index = andPlayIndex {
            player.fetchAndPlayPlayerItem(index)
        }
    }

    //MARK:- Player DataSource
    func hysteriaPlayerNumberOfItems() -> Int {
        //print("player need count")
        return playlist.count
    }

    func hysteriaPlayerAsyncSetUrlForItemAtIndex(index: Int, preBuffer: Bool) {
        HysteriaPlayer.sharedInstance().setupPlayerItemWithUrl(NSURL(string: playlist[index].url),
                                                               index: index)
    }

    //MARK:- Player Delegate
    func hysteriaPlayerWillChangedAtIndex(index: Int) {
        print("player will changed track with index : \(index)")
        resetRemotePlayingTime()
        selectedSong.value = playlist[index]
    }

    func hysteriaPlayerCurrentItemChanged(item: AVPlayerItem!) {
        updateAlbumCover()
    }

    func hysteriaPlayerItemPlaybackStall(item: AVPlayerItem!) {
        print("player freeze loading item")
        //TODO: place loading indicator
    }

    func hysteriaPlayerDidReachEnd() {
        print("player end playlist")
        playerStopped.value = true
        selectedSong.value = nil
    }

    func hysteriaPlayerReadyToPlay(identifier: HysteriaPlayerReadyToPlay) {
        print("player did load item")
        if identifier == .CurrentItem {
            resetRemotePlayingTime()
            if player.getHysteriaPlayerStatus() != .ForcePause {
                player.play()
                addTimeObserver()
            }
        }
    }

    func hysteriaPlayerDidFailed(identifier: HysteriaPlayerFailed, error: NSError!) {
        print("player error")
        errorMessages.next(String(error.userInfo["NSLocalizedDescription"] ?? "unknown message"))
        playerStopped.value = true
        selectedSong.value = nil
    }


    func hysteriaPlayerItemFailedToPlayEndTime(item: AVPlayerItem!, error: NSError!) {
        print("player error")
        errorMessages.next(String(error.userInfo["NSLocalizedDescription"] ?? "unknown message"))
        playerStopped.value = true
        selectedSong.value = nil
    }


//MARK:- Current playing time update
    func removeTimeObserver() {
        if let obs = timeObserver {
            HysteriaPlayer.sharedInstance().removeTimeObserver(obs)
            timeObserver = nil
        }
    }

    func addTimeObserver() {
        removeTimeObserver()
        timeObserver = HysteriaPlayer.sharedInstance()
            .addPeriodicTimeObserverForInterval(CMTimeMake(1, 2),
                                                queue: dispatch_get_main_queue()) {
                [unowned self] time in
                if !self.stopUpdateCurrentTime {
                    self.currentTime.value = self.player.getPlayingItemCurrentTime()
                }
            }
    }

    //MARK:- Remote control update
    func resetRemotePlayingTime() {
        synced(infoDict) {
            let playbackRate = self.player.isPlaying() ?  1.0 : 0.0
            let elapsedTime = (self.player.getCurrentItem() == nil) ?
                Double(0.0) :
                Double(self.player.getPlayingItemCurrentTime())
            self.infoDict[ MPNowPlayingInfoPropertyPlaybackRate ] = playbackRate
            self.infoDict[ MPNowPlayingInfoPropertyElapsedPlaybackTime ] = elapsedTime
            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = self.infoDict
        }

    }

    func updateAlbumCover() {
        guard let song = self.selectedSong.value else { return }

        if let coverImg = coversCache[song.url] as? UIImage {
            cover.value = coverImg
        } else {
            cover.value = nil
            let asset = HysteriaPlayer.sharedInstance().getCurrentItem().asset
            asset.loadValuesAsynchronouslyForKeys(["commonMetadata"]) {
                let artworks = AVMetadataItem
                    .metadataItemsFromArray(asset.commonMetadata,
                                            withKey: AVMetadataCommonKeyArtwork,
                                            keySpace: AVMetadataKeySpaceCommon)
                for item in artworks {
                    if let img = self.getCoverFromMetaData(item) {
                        self.coversCache[song.url] = img
                        if song == self.selectedSong.value {
                            self.cover.value = img
                        }
                        break
                    }
                }
            }
        }
    }

    func getCoverFromMetaData(item: AVMetadataItem) -> UIImage? {
        var ret: UIImage? = nil
        if item.keySpace == AVMetadataKeySpaceID3 {
            if let data = item.value?.copyWithZone(nil) as? NSData {
                ret = UIImage(data: data)
            }
        } else if item.keySpace == AVMetadataKeySpaceiTunes {
            if let data = item.value?.copyWithZone(nil) as? NSData {
                ret = UIImage(data: data)
            }
        }
        return ret
    }

//MARK:- Deinitialization
    deinit {
        removeTimeObserver()
    }
}
