//
//  NMCFriendsVM.swift
//  NastyaMusic
//
//  Created by adm on 06.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import Bond

protocol NMCSegueDataProcessing {
    func setSegueData(data: AnyObject)
}

class NMCFriendsVM: NSObject, NMCSegueDataProcessing {

    let sourceData = ObservableArray<NMCFriendItem>()
    let loading = Observable<Bool>(false)
    let errorMessages = EventProducer<String>()


    func loadData() {
        self.loading.value = true

        NMCCacheControl.sharedManager.getFriends({ (result) in
            self.sourceData.removeAll()
            self.sourceData.insertContentsOf(result, atIndex: 0)
            self.loading.value = false

            }, errorHandler: { (error) in
                self.errorMessages.next(String(error.userInfo["NSLocalizedDescription"] ??
                                                "unknown message"))
                self.loading.value = false
        })

    }

    func setSegueData(data: AnyObject) {}
}
