//
//  NMCSongsVM.swift
//  NastyaMusic
//
//  Created by adm on 07.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import Bond

enum NMCSongsVCType { case Group, Friend, MySongs, Unknown }


class NMCSongsVM: NSObject, NMCSegueDataProcessing, NMCTableVM {

    var masterId: Int?
    var vcType = NMCSongsVCType.Unknown

    let sourceData = ObservableArray<NMCSongItem>()
    let loading = Observable<Bool>(false)
    let errorMessages = EventProducer<String>()
    let noDataText = Observable<String>("")
    let selectedIndex = Observable<NSIndexPath?>(nil)

    var songSubscription: DisposableType!

    override init() {
        super.init()
        bindToPlayer()
    }

    func bindToPlayer() {
        let player = NMCPlayer.sharedManager

        songSubscription = player.selectedSong.map { $0?.url }
            .observe { [unowned self] optUrl in
                guard let url = optUrl else {
                    self.selectedIndex.value = nil
                    return
                }
                if let foundSongIndex = self.sourceData.array.indexOf({ $0.url == url}) {
                    self.selectedIndex.value = NSIndexPath(forRow: foundSongIndex, inSection: 0)
                } else {
                    self.selectedIndex.value = nil
                }
            }
    }

    func loadData() {
        guard vcType == NMCSongsVCType.MySongs || masterId != nil else {
            let chooseSubstring = (vcType == NMCSongsVCType.Friend) ? "друга" : "группу"
            noDataText.value = String(format:"Выберите %@", chooseSubstring)
            return
        }

        self.loading.value = true

        let idString: String? = (masterId == nil) ? nil : String(masterId!)
        let ownerString = (vcType == NMCSongsVCType.Group) ? "-\(idString!)" : idString

        NMCCacheControl.sharedManager.getSongs(ownerString, albumId: nil, completion: { (result) in
            self.sourceData.removeAll()
            self.sourceData.insertContentsOf(result, atIndex: 0)
            self.loading.value = false

            }, errorHandler: { (error) in
                self.errorMessages.next(String(error.userInfo["NSLocalizedDescription"] ??
                                                "unknown message"))
                self.loading.value = false
        })

    }

    func tableDidSelectItemAtIndexPath(indexPath: NSIndexPath) {
        let player = NMCPlayer.sharedManager
        if (selectedIndex.value == nil) || (selectedIndex.value != indexPath) {
            selectedIndex.value = indexPath
            player.resetWithPlaylist(sourceData.array, andPlayIndex:selectedIndex.value?.row)
            if !player.isPlaying.value {
                player.play()
            }
        } else {
            if player.isPlaying.value {
                player.pause()
            } else {
                player.play()
            }
        }
    }

    func setSegueData(data: AnyObject) {}

    deinit {
        songSubscription.dispose()
    }
}
