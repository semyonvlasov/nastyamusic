//
//  NMCSongItem.swift
//  NastyaMusic
//
//  Created by adm on 26.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import CoreData

class NMCSongItem: NMCInitModelWithDict, NMCRemoteEntity, Equatable {
    static let entityRequestMethod = "audio.get"

    let url: String
    let title: String
    let artist: String
    let id: Int
    let duration: Int

    required init?(dict: NMCModeLDict) {
        guard let url = dict["url"] as? String,
                id = dict["id"] as? Int
        else {
            print("fail to init song model with dict: " + dict.description)
            return nil
        }
        self.url = url
        self.id = id
        self.artist = (dict["artist"] as? String) ?? "UnknownArtist"
        self.title = (dict["title"] as? String) ?? "NoTitle"
        self.duration = (dict["duration"] as? Int) ?? 0
    }
}

func == (lhs: NMCSongItem, rhs: NMCSongItem) -> Bool {
    return lhs.url == rhs.url
}
