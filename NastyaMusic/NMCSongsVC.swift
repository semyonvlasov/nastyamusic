//
//  NMCSongsVC.swift
//  NastyaMusic
//
//  Created by adm on 22.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit
import VK_ios_sdk

class NMCSongsVC: UIViewController, NMCMVVMController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!

    var vm: NMCSegueDataProcessing { return viewModel }

    private let viewModel = NMCSongsVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()

        viewModel.loadData()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        addNavigationBtn()
    }

    private func bindViewModel() {
        viewModel.sourceData.lift()
            .bindTo(tableView) { [unowned self] indexPath, dataSource, tableView in
                let cell = self.tableView.dequeueReusableCellWithIdentifier("cellIdSong",
                                                                            forIndexPath: indexPath)
                let song = dataSource[indexPath.section][indexPath.row]
                cell.textLabel?.text =  song.artist + " " +
                                        song.title + " " +
                                        song.duration.durationFormat()
                return cell
            }

        viewModel.selectedIndex.observe { [unowned self] index in
            let oldIndex = self.tableView.indexPathForSelectedRow
            if oldIndex != index {
                if let newIndex = index {
                    self.tableView.selectRowAtIndexPath(newIndex,
                                                        animated: true,
                                                        scrollPosition: .None)
                } else {
                    self.tableView.deselectRowAtIndexPath(oldIndex!, animated: true)
                }
            }
        }

        viewModel.loading.observe { [unowned self] loading in
            if loading {
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            } else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        }

        viewModel.errorMessages.observe { error in
            UIAlertView(title: "error",
                                    message: error,
                                    delegate: nil,
                                    cancelButtonTitle: "OK")
                .show()
        }

        viewModel.noDataText.bindTo(noDataLbl.bnd_text)
        viewModel.noDataText.map { (String($0) == "")}.bindTo(noDataLbl.bnd_hidden)
        viewModel.noDataText.map { (String($0) != "")}.bindTo(tableView.bnd_hidden)
    }

    func addNavigationBtn() {
    guard let split = self.splitViewController else {return}
        if !split.collapsed {
            self.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem()
        }
    }

    //MARK:- Table view delegate
    func tableView(tableView: UITableView,
                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        viewModel.tableDidSelectItemAtIndexPath(indexPath)
    }
}
