//
//  NMCShortPlayerVC.swift
//  NastyaMusic
//
//  Created by adm on 22.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit

class NMCPlayerVC: UIViewController {

    @IBOutlet weak var coverImgView: UIImageView!

    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var smallCoverImgView: UIImageView!
    @IBOutlet weak var songLbl: UILabel!
    @IBOutlet weak var artistLbl: UILabel!

    @IBOutlet weak var curTimeLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!

    @IBOutlet weak var pauseBtn: UIButton!

    private var sliderBusy = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configurePopup()
        addGestures()
        bindWithPlayer()
        bindViewModel()
    }

    func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(exitFromVC))
        headerView.addGestureRecognizer(tapGesture)

        let sliderTapRecognizer = UITapGestureRecognizer(target: self,
                                                         action: #selector(sliderTappedAction))
        progressSlider.addGestureRecognizer(sliderTapRecognizer)
    }

    func bindViewModel() {
        //TODO: add playlist actions
    }

    func bindWithPlayer() {
        let player = NMCPlayer.sharedManager

        player.selectedSong.ignoreNil().observe { [unowned self] song in
            self.popupItem.title = song.title
            self.songLbl.text = song.title

            self.popupItem.subtitle = song.artist
            self.artistLbl.text = song.artist

            self.durationLbl.text = song.duration.durationFormat()
        }

        player.cover.map { $0 ?? UIImage(named:"headphones_large")}
            .bindTo(coverImgView.bnd_image)
        player.cover.map { $0 ?? UIImage(named: "headphones_small")}
            .bindTo(smallCoverImgView.bnd_image)

        player.currentTime.map { Int($0).durationFormat() }.bindTo(curTimeLbl.bnd_text)

        player.currentTime
            .combineLatestWith(player.selectedSong.ignoreNil())
            .map { $0 / Float($1.duration)}.observe { [unowned self] progress in
            if !self.sliderBusy {
                self.progressSlider.setValue(progress, animated: true)
            }
            self.popupItem.progress = progress
        }

        player.errorMessages.observe { error in
            UIAlertView(title: "error",
                                    message: error,
                                    delegate: nil,
                                    cancelButtonTitle: "OK")
                .show()
        }
    }

    func configurePopup() {
        let player = NMCPlayer.sharedManager

        let pause = makeButtonWithImageName("pause22",
                                            action: #selector(playPause))
        let list = makeButtonWithImageName("list22",
                                           action: #selector(showWithPlaylist))

        if UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Pad {
            let prev = makeButtonWithImageName("prevTrack22",
                                               action: #selector(prevTrack))
            let next = makeButtonWithImageName("nextTrack22",
                                               action: #selector(nextTrack))
            let space1 = makeButtonWithImageName("empty", action: nil)
            let space2 = makeButtonWithImageName("empty", action: nil)

            self.popupItem.leftBarButtonItems = [ prev, pause, next ]
            self.popupItem.rightBarButtonItems = [ space1, space2, list ]
        } else {
            self.popupItem.leftBarButtonItems = [ pause ]
            self.popupItem.rightBarButtonItems = [ list ]
        }

        player.isPlaying.observe { [unowned self] isPlaying in
            pause.image = isPlaying ? UIImage(named: "pause22") : UIImage(named: "play22")
            self.pauseBtn.setImage( UIImage(named: isPlaying ? "pause44" : "play44"),
                                    forState: .Normal)
        }
    }

    private func makeButtonWithImageName(imageName: String,
                                         action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(image: UIImage(named: imageName),
                               style: .Plain,
                               target: self,
                               action: nil)
    }

    @IBAction func playPause(sender: AnyObject) {
        let player = NMCPlayer.sharedManager
        if player.isPlaying.value {
            player.pause()
        } else {
            player.play()
        }
    }

    @IBAction func nextTrack(sender: AnyObject) {
        NMCPlayer.sharedManager.playNext()
    }

    @IBAction func prevTrack(sender: AnyObject) {
        NMCPlayer.sharedManager.playPrevious()
    }

    func showWithPlaylist() {
        popupPresentationContainerViewController?
            .openPopupAnimated(true, completion: { self.showWithPlaylist()})
    }

    @IBAction func showHidePlaylist(sender: AnyObject) {

    }

    @IBAction func scrubBegin(sender: AnyObject) {
        sliderBusy = true
    }

    @IBAction func scrubEnd(sender: AnyObject) {
        NMCPlayer.sharedManager.seekWithProgress(progressSlider.value) { succes in
            self.sliderBusy = false
        }
    }

    @IBAction func sliderTappedAction(sender: UITapGestureRecognizer) {
        if let slider = sender.view as? UISlider {
            sliderBusy = true

            if slider.highlighted { return }

            let point = sender.locationInView(slider)
            let percentage = Float(point.x / slider.bounds.width)
            let delta = percentage * (slider.maximumValue - slider.minimumValue)
            let value = slider.minimumValue + delta
            slider.setValue(value, animated: true)

            NMCPlayer.sharedManager.seekWithProgress(progressSlider.value) { succes in
                self.sliderBusy = false
            }
        }
    }

    func exitFromVC() {
        popupPresentationContainerViewController?
            .closePopupAnimated(true, completion: nil)
    }
}
