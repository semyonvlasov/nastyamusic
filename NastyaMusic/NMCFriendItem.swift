//
//  NMCFriendItem.swift
//  NastyaMusic
//
//  Created by adm on 06.05.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import CoreData

class NMCFriendItem: NMCInitModelWithDict, NMCRemoteEntity {
    static let entityRequestMethod = "friends.get"

    let id: Int
    let firstName: String
    let lastName: String
    let photoUrl: String?

    required init?(dict: NMCModeLDict) {
        guard let id = dict["id"] as? Int
        else {
            print("fail to init friend model with dict: " + dict.description)
            return nil
        }
        self.id = id
        self.firstName = (dict["first_name"] as? String) ?? ""
        self.lastName = (dict["last_name"] as? String) ?? ""
        self.photoUrl = (dict["photo_100"] as? String)
    }
}
