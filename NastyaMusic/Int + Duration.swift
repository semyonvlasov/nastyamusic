//
//  Int + Duration.swift
//  NastyaMusic
//
//  Created by adm on 07.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation

extension Int {
    func durationFormat() -> String {
        let hours = Int(self / 3600)
        let minutes = Int((self - hours * 3600) / 60)
        let seconds = self - hours * 3600 - minutes * 60

        let hoursStr = hours > 0 ? String(hours) + ":" : ""
        let minutesStr = hours > 0 ? String(format: "%02d", minutes) : String(minutes)
        let secondsStr = String(format: "%02d", seconds )
        return "\(hoursStr)\(minutesStr):\(secondsStr)"
    }
}
