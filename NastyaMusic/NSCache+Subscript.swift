//
//  NSCache+Subscript.swift
//  NastyaMusic
//
//  Created by adm on 29.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation

extension NSCache {
    subscript(key: AnyObject) -> AnyObject? {
        get {
            return objectForKey(key)
        }
        set {
            if let value: AnyObject = newValue {
                setObject(value, forKey: key)
            } else {
                removeObjectForKey(key)
            }
        }
    }
}
