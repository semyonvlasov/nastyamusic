//
//  CommonProtocols.swift
//  NastyaMusic
//
//  Created by adm on 07.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation


extension UIViewController {
    func isIPAD() -> Bool {
        return UIDevice.currentDevice().userInterfaceIdiom == .Pad
    }
}
