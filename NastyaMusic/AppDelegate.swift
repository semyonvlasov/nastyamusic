//
//  AppDelegate.swift
//  NastyaMusic
//
//  Created by adm on 21.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import UIKit
import VK_ios_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }

    func applicationWillResignActive(application: UIApplication) {

    }

    func applicationDidEnterBackground(application: UIApplication) {

    }

    func applicationWillEnterForeground(application: UIApplication) {

    }

    func applicationDidBecomeActive(application: UIApplication) {

    }

    func applicationWillTerminate(application: UIApplication) {

    }

    // MARK: - VK set url
    // IOS 9
    func application(app: UIApplication,
                     openURL url: NSURL,
                             options: [String : AnyObject]) -> Bool {
        if #available(iOS 9.0, *) {
            let srcApplication = options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String
            VKSdk.processOpenURL(url, fromApplication:srcApplication)
        }
        return true
    }

    // IOS 8
    func application(application: UIApplication,
                     openURL url: NSURL, sourceApplication: String?,
                        annotation: AnyObject) -> Bool {
        VKSdk.processOpenURL(url, fromApplication:sourceApplication)
        return true
    }
}
