//
//  NMCCacheControl.swift
//  NastyaMusic
//
//  Created by adm on 29.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import VK_ios_sdk

let NMCDefaultAlbumAllSongs = "NMCDefaultAlbumAllSongs"
let NMCDefaultAlbumWallSongs = "NMCDefaultAlbumWallSongs"


typealias NMCModeLDict = Dictionary<String, AnyObject>


protocol NMCInitModelWithDict {
    init?(dict: NMCModeLDict)
}


protocol NMCRemoteEntity {
    static var entityRequestMethod: String { get }
}

class NMCCacheControl: NSObject {
    static let sharedManager = NMCCacheControl()

    func getFriends(completion: ((result: [NMCFriendItem]) -> Void)?,
                    errorHandler: ((mes: NSError) -> Void)? ) {
        let params = ["fields" : "photo_100", "order" : "name"]
        loadItemsList(NMCFriendItem.self,
                      params: params,
                      completion: completion,
                      errorHandler: errorHandler,
                      logResult: false)
    }

    func getGroups(completion: ((result: [NMCGroupItem]) -> Void)?,
                   errorHandler: ((mes: NSError) -> Void)? ) {
        let params = ["extended" : true]
        loadItemsList(NMCGroupItem.self,
                      params: params,
                      completion: completion,
                      errorHandler: errorHandler,
                      logResult: false)
    }

    func getSongs(ownerId: String?,
                  albumId: String?,
                  completion: ((result: [NMCSongItem]) -> Void)?,
                  errorHandler: ((mes: NSError) -> Void)? ) {
        var params = [NSObject : AnyObject]()
        if ownerId != nil { params["owner_id"] = ownerId! }
        if albumId != nil { params["album_id"] = albumId! }
        loadItemsList(NMCSongItem.self,
                      params: params,
                      completion: completion,
                      errorHandler: errorHandler,
                      logResult: false)
    }

//MARK:- Private methods
    private func loadItemsList<T where T: NMCInitModelWithDict, T: NMCRemoteEntity>(type: T.Type,
                                                    params: [NSObject: AnyObject]?,
                                                    completion: ((result: [T]) -> Void)?,
                                                    errorHandler: ((mes: NSError) -> Void)?,
                                                    logResult: Bool) {
        let params = (params?.count > 0) ? params : nil
        let req: VKRequest = VKRequest(method: type.entityRequestMethod, parameters: params)

        req.executeWithResultBlock({ (response) -> Void in
            let entities = self.parseResult(response, type: type)
            if logResult { print(entities.description)}
            if let completion = completion { completion(result: entities) }
            }, errorBlock: {
                (errorMessage) -> Void in
                print(errorMessage)
                if let errorHandler = errorHandler { errorHandler(mes : errorMessage)}
        })
    }

    private func parseResult<T: NMCInitModelWithDict>(response: VKResponse, type: T.Type) -> [T] {
        var ret = [T]()
        guard let responseDict = response.json as? NSDictionary,
                itemsArr = responseDict["items"] as? Array<NMCModeLDict> else {
            print ("can't  access array of items")
            return ret
        }
        for item: Dictionary in itemsArr {
            if let mappedItem = type.init(dict : item) {
                ret.append(mappedItem)
            }
        }
        return ret
    }
}
