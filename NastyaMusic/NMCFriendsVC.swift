//
//  NMCFriendsVC.swift
//  NastyaMusic
//
//  Created by adm on 22.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit
import Nuke

protocol NMCMVVMController {
    var vm: NMCSegueDataProcessing { get }
}

protocol NMCTableVM {
    func tableDidSelectItemAtIndexPath(indexPath: NSIndexPath)
}

class NMCFriendsVC: UIViewController, NMCMVVMController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!

    var vm: NMCSegueDataProcessing { return viewModel }

    private let viewModel = NMCFriendsVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()

        viewModel.loadData()
    }

    private func bindViewModel() {
        viewModel.sourceData.lift()
            .bindTo(tableView) { [unowned self] indexPath, dataSource, tableView in
                let cell = self.tableView.dequeueReusableCellWithIdentifier("cellIdFriend",
                                                                            forIndexPath: indexPath)
                let friend = dataSource[indexPath.section][indexPath.row]
                cell.textLabel?.text = friend.firstName + " " + friend.lastName
                cell.imageView?.contentMode = .ScaleAspectFit
                cell.imageView?.image = UIImage(named: "friend")

                if let urlStr = friend.photoUrl,
                    imageUrl = NSURL(string: urlStr) {
                    cell.imageView?.nk_setImageWith(imageUrl)
                }

                return cell
            }

        viewModel.loading.observe { [unowned self] loading in
            if loading {
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            } else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        }

        viewModel.errorMessages.observe { error in
            UIAlertView(title: "error",
                                    message: error,
                                    delegate: nil,
                                    cancelButtonTitle: "OK")
                .show()
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if  !isIPAD() {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard   segue.identifier == "segueIdFriendSongs",
            let nc = segue.destinationViewController as? UINavigationController,
                vc = nc.viewControllers[0] as? NMCMVVMController,
                destVm = vc.vm as? NMCSongsVM
        else {
            print("wrong destination vc or row doesn't selected")
            return
        }

        let selectedPath = tableView.indexPathForSelectedRow
        destVm.masterId = (selectedPath != nil) ? viewModel.sourceData[selectedPath!.row].id : nil
        destVm.vcType = NMCSongsVCType.Friend

    }
}
