//
//  NMCGroupsVC.swift
//  NastyaMusic
//
//  Created by adm on 22.04.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation
import UIKit


class NMCGroupsVC: UIViewController, NMCMVVMController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!

    var vm: NMCSegueDataProcessing { return viewModel }

    private let viewModel = NMCGroupsVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()

        viewModel.loadData()
    }

    private func bindViewModel() {
        viewModel.sourceData.lift()
            .bindTo(tableView) { [unowned self] indexPath, dataSource, tableView in
                let cell = self.tableView.dequeueReusableCellWithIdentifier("cellIdGroup",
                                                                            forIndexPath: indexPath)
                let group = dataSource[indexPath.section][indexPath.row]
                cell.textLabel?.text = group.name

                if let urlStr = group.photoUrl,
                    imageUrl = NSURL(string: urlStr) {
                    cell.imageView?.nk_setImageWith(imageUrl)
                }

                return cell
            }

        viewModel.loading.observe { [unowned self] loading in
            if loading {
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            } else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        }

        viewModel.errorMessages.observe { error in
            UIAlertView(title: "error",
                                    message: error,
                                    delegate: nil,
                                    cancelButtonTitle: "OK")
                .show()
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if  !isIPAD() {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard   segue.identifier == "segueIdGroupSongs",
            let nc = segue.destinationViewController as? UINavigationController,
                vc = nc.viewControllers[0] as? NMCMVVMController,
                destVm = vc.vm as? NMCSongsVM
        else {
            print("wrong destination vc or row doesn't selected")
            return
        }
        let selectedPath = tableView.indexPathForSelectedRow
        destVm.masterId = (selectedPath != nil) ? viewModel.sourceData[selectedPath!.row].id : nil
        destVm.vcType = NMCSongsVCType.Group
    }
}
