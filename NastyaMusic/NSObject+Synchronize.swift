//
//  NSObject+Synchronize.swift
//  NastyaMusic
//
//  Created by adm on 14.06.16.
//  Copyright © 2016 svv. All rights reserved.
//

import Foundation

extension NSObject {
    var timestamp: NSTimeInterval {
        return NSDate().timeIntervalSince1970 * 1000
    }

    func synced(lock: AnyObject, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
}
